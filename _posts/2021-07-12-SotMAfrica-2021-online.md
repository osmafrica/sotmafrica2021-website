---
layout: post
title:  State of the Map Africa 2021 will be held virtually
date:   2021-07-15 08:00:00
image:  sotmafrica-online.jpg
isStaticPost: false
---
![](../img/posts/sotmafrica-online.jpg)

State of the Map Africa 2021, which was meant to be in Nairobi, Kenya will now be held virtually online on the same dates 19–21 November 2021. The decision to go virtual is the safest approach for the well-being of our local and global community and we are excited that a virtual event makes it possible to broaden the learning opportunities and engagement with the community at this time.

The virtual conference will include our regular offerings and opportunities for engagement, including sessions, workshops, and keynotes, and space to connect with peers from around the globe.

The call for session proposals is still open, and we would like to hear from all of you, please submit your 5 or 20 minutes talks, panel sessions, or workshops. Whether you are a beginner or experienced mapper, an OSM data user, or a community leader, we would like to hear from you. Submit your proposal now http://2021.stateofthemap.africa/calls/

The theme for our conference this year is Leaving no one behind: Emerging from a global pandemic. We are looking forward to meeting you all online come November!

