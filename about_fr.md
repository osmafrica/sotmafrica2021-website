---
layout: post
title: SotM Africa & OpenStreetMap Africa
permalink: /fr/about/
isStaticPost: true
image: bassam_jump.jpg
---
__[English Version](/about/)__

![State of the Map 2019 Photo](/img/posts/sotm2019_bassam.jpg)

> Ne laisser personne pour compte: Émergence d'une pandémie mondiale

<< State of the Map Africa >>  (SotM Africa) est une conférence régionale biannuelle qui célèbre la culture de la cartographie ouverte, des données ouvertes, des SIG et de leur impact à travers l'Afrique. La première conférence SotM Africa a été organisée par la communauté OSM en [Ouganda en 2017](https://wiki.openstreetmap.org/wiki/State_of_the_Map_Africa_2017). [En 2019, elle a été accueillie à Abidjan et à Grand-Bassam en Côte d'Ivoire.](https://2019.stateofthemap.africa)  Le premier jour, la conférence a été organisée conjointement avec le programme "Understanding Risk West and Central Africa" de la Banque mondiale. La conférence de cette année continuera à s'appuyer sur la stratégie envisagée pour OpenStreetMap en Afrique, en tant que réseau renouvelé, fort et en pleine croissance, et dans le cadre du mouvement mondial OpenStreetMap et Open GIS.

La conférence de cette année s'appuiera sur une nouvelle stratégie envisagée pour OpenStreetMap Africa en tant que réseau renouvelé, fort et en croissance, et faisant partie du mouvement mondial de cartographie ouverte.

La conférence de cette année offrira un espace diversifié et amusant pour que chacun puisse collaborer et faire partie de la communauté OpenStreetMap Africa. Nous poserons également les jalons qui nous fourniront une base solide pour la communauté ouverte, collaborative et participative que nous voulons construire et qui est nécessaire pour que notre mouvement se diversifie et s'épanouisse. Nous espérons que vous vous joindrez à nous. 

#### OpenStreetMap Africa

OSM Africa est un réseau de communautés locales OpenStreetMap, des communautés de toute l'Afrique qui se donnent la main pour partager des ressources et collaborer pour se développer et produire une carte complète et bien détaillée de l'Afrique sur OpenStreetMap afin de faire progresser la qualité, l'exhaustivité et la durabilité des données géospatiales en Afrique.