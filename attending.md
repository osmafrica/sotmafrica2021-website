---
layout: post
title: Attending SotM Africa 2021
permalink: /attending/
isStaticPost: true
image: nairobi.jpg
---

__[Version française](/fr/attending/)__

#### About Nairobi
Nairobi originated in the late 1890s as a colonial railway settlement. It grew up around a railway line constructed by the British colonial officials from Mombasa on the Indian Ocean coast to Uganda. The present site of Nairobi was selected as a stores' depot, shunting yard and camping ground for the thousands of Indian labourers and British colonials. 
It takes its name from the Maasai phrase Enkare nyirobi which translates to "cool water", a reference to it formerly being a swamp and the Nairobi river which flows through the city. However, it is popularly known as the ‘Green City in the Sun’

Nairobi is the capital and the largest city of Kenya, the most economically empowered city and a major business hub in East Africa. The metropolitan urban center (1°09′S 36°39′E and 1°27′S 37°06′E)  has an estimated population of 4,397,073, according to the 2019 Population Census. 

#### Attractions
Seeing wildlife with a backdrop of the city skyline makes Nairobi special as it is the only capital city in the world with a national park. With a 15-minute drive from the skyscrapers of the city centre, you can enjoy a classic African wildlife experience at Nairobi National Park. Lion, cheetah, zebra, wildebeest, giraffe, rhinoceros, and buffalo roam the sun-soaked savanna here. Animal lovers can cuddle and feed baby elephants at David Sheldrick’s trust, best known for protecting orphaned baby elephants, and connect with giraffes at Giraffe centre. It is also the home to the animal orphanage, Mamba(crocodile) village, Karura forest, Bomas of Kenya, Nairobi Museum, and Snake park among others.

#### Visa Policy for Kenya
Visa regulations to travel to Kenya states that citizens of approximately 45 countries can visit the country without the need to obtain a visa depending on the duration and purpose of the planned stay in Kenya. Kenya doesn’t offer foreign citizens the option of obtaining a visa on arrival but allows up to 140 countries and territories around the world to obtain a single entry electronic visa through an online application at least 7 days in advance of travel.
Citizens of an additional 60 countries are required to obtain a visa from a government office, either an embassy or consulate.
See full list of the various categories here [Visa Policy for Kenya](https://www.onlinevisa.com/visa-policy/kenya/)

#### Transportation

#### Getting to Nairobi 
##### By Air
Nairobi hosts Jomo Kenyatta International Airport (JKIA), Africa’s premier hub and ideal getaway into and out of East and Central Africa and a central hub of aviation activity for the region. It is served by over 40 passenger airlines, with direct connections to Africa, Asia, Europe, the Middle East and the United States.


###### From the Airport to the hotels 
JKIA is just 18kms away from the city centre and one can easily connect using the public transport or taxi services.
The official taxis from the Airport are yellow in color. 
However using cab-hailing services like Uber and Bolt is cheaper. 


##### Buses
- Neighbouring countries within Eastern, Central, and Southern Africa can travel to Nairobi using public bus services.

##### Transportation around the city
There are several options for getting to the venue and getting around Nairobi:

###### Public transport 
Buses and minibuses referred to locally as “matatu”, are the primary mode of transport within Nairobi. Each bus serves a specific route, with the start and end of each trip being the Nairobi Central Business District; therefore, one might take two different buses to connect from one point to another. Trips can only be paid for by cash, and one would need to be aware of the bus stops and where they plan to alight.

###### Taxis
Taxis from a taxi stand are often expensive, but it's much easier to use a cab-hailing mobile service. Nairobi has Uber, Bolt, and its own local option, Little. 

###### Motorbikes
Using motorbikes, commonly known as ‘boda boda’ is one of the quickest ways to get about the city. It's possible to request a motorbike using mobile apps: Uber, Bolt, Little and SafeBoda.

#### Accomodation
As a popular city for tourists, Nairobi provides plenty of options for accommodations for just short bus rides, taxi, or walk to and from our conference venues. 

We boast of a variety of hotels ranging from 5-star hotels like Villa Rosa Kempinski, Intercontinental hotel, Boma inn, Hilton Hotel, Golden Tulip, Ibis Styles among others down to 1-star hotels that offer only the essentials while still meeting reasonable hygiene and security standards all depending on one's budget and preferences. 

Airbnb is available throughout the city from as little as $9 per night and can also accommodate larger groups. We also have hostels and dormitories options provided by The Kenya Young Men Christian Association(YMCA) ranging from shared dorms to private single or double rooms and is a walking distance to and from Nairobi CBD. 

#### Food
Kenya’s staple and most common food is a cornmeal starch made into a thick paste known as ugali. It goes down well with fried vegetables or any kind of meat stew and is available anywhere and everywhere, from sit down restaurants to Kenya street foods and stalls around the country. Other than ugali, most Kenyan dishes also embrace the country’s multi-racial nature and many other dishes loved by different countries can be found in Kenyan restaurants. The meals are filling and inexpensive and from as low as $1 one can have a fulfilling dish.